import React from "react";
import logo from "./Leonhard_Euler.jpg";

function Component(props) {
  return <h1>{props.title}</h1>;
}
const text = "Leonlord Euler";
function Eulerpicture() {
  return (
    <div>
      <div class="center"></div>
      <img src={logo} alt="Logo" height={700} width={700} />
      <p>
        Leonhard Euler (statement: [ ˈɔʏlɐ ], 15 April 1707 Basel - 18 September
        ( J : 7 September) 1783 Saint Petersburg ) [1] was a Swiss mathematician
        and physicist who spent most of his life in Russia and Prussia
        (present-day Germany). [2] He is considered one of the greatest
        mathematicians of all time. Euler was the first to use the concept of
        function ( Gottfried Leibniz defined the concept in 1694). In addition
        to this, Euler started, among other things, network theoryand examining
        the fundamentals of variance calculus . He also derives many of the
        notations currently in use, including the notation of sum , silicon ,
        and imaginary unit .
      </p>
      <a href="https://fi.wikipedia.org/wiki/Leonhard_Euler">Euler's link</a>
    </div>
  );
}

const App = () => {
  return (
    <div>
      <Component title={text} />
      <Eulerpicture />
    </div>
  );
};
export default App;
