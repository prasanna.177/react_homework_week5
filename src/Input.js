import React, { useState } from "react";
const Input = (props) => {
  const [name, setName] = useState(props.name);
  const handleInputChange = (ev) => {
    setName(ev.currentTarget.value);
  };
  return (
    <div>
      <div>{name}</div>
      <input onChange={handleInputChange} />
    </div>
  );
};
export default Input;
