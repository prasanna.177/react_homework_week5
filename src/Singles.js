function Single(props) {
  return (
    <div>
      <p>
        <b>Title: {props.title}</b>
      </p>
      <p>Album: {props.album}</p>
      <p>Year: {props.year}</p>
    </div>
  );
}
const singles = [
  { title: "Du hast", album: "Sehnsucht", year: 1997 },
  { title: "Ich will", album: "Mutter", year: 2001 },
  { title: "Feuer frei", album: "Mutter", year: 2002 },
  { title: "Amerika", album: "Reise, Reise", year: 2004 },
  { title: "Mann gegen Mann", album: "Rosenrot", year: 2005 },
  { title: "Deutschland", album: "Untitled", year: 2019 },
  { title: "Zick Zack", album: "Zeit", year: 2022 },
];

const Singles = () => {
  const items = singles.map((single) => {
    return (
      <Single title={single.title} album={single.album} year={single.year} />
    );
  });
  return <div>{items}</div>;
};
export default Singles;
